setInterval(displayTime, 500);

function displayTime()
{
	var date = new Date();
	//Get the current hours, minutes & seconds
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

	//Format it nicely
	if(seconds >= 10)
	var timeString = hours + ":" + minutes + ":" + seconds;
	else
	var timeString = hours + ":" + minutes + ":0" + seconds;

	//Add the time to the div with ID "time"
	document.getElementById('time').innerHTML = timeString;
}